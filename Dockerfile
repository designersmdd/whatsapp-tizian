FROM node:14-alpine as base

WORKDIR .
COPY package*.json /
EXPOSE 8000

FROM base as production
ENV NODE_ENV=production
RUN npm ci
COPY . /
CMD ["node", "server"]