/* SOCKET SETUP */

const socket = io(`${window.location.hostname}:8000`);
const myUsername = document.getElementById("myUsername");

socket.on("connect", () => {
  console.log("My connection id is: ", socket.id);

  const myName = localStorage.getItem("username");

  if (myName === null) {
    socket.emit("requestUsername");
  } else {
    socket.emit("myUsername", myName);
    myUsername.innerHTML = myName;
  }
});

socket.on("newUsername", (username) => {
  localStorage.setItem("username", username);
  myUsername.innerHTML = username;
});

/* VARIABLES */

let currentChatId = undefined;

const chatMessages = new Map();

/* INPUT HANDLING */

const messageForm = document.getElementById("messageForm");
const messageInput = document.getElementById("messageInput");

messageForm.onsubmit = (event) => {
  event.preventDefault();

  if (currentChatId === undefined) {
    alert("You have no chat selected");
  }

  const toSendValue = messageInput.value;

  messageInput.value = "";

  socket.emit("messageReceived", toSendValue, currentChatId);

  rememberChatMessage(currentChatId, toSendValue, socket.id);

  printMessage(toSendValue, socket.id);
};

/* MESSAGES DISPLAY */

const printMessage = (message, messageSender) => {
  const messageElement = document.createElement("li");
  const textElement = document.createElement("div");

  let position = "";
  let background = "";

  if (messageSender === socket.id) {
    console.log("its me");
    position = "justify-end";
    background = "bg-gray-100";
  } else {
    position = "justify-start";
    background = "bg-gray-300";
  }

  textElement.innerHTML = message;
  textElement.classList = "inline-block rounded-lg px-3 py-1";
  textElement.classList.add(background);

  messageElement.classList = "my-1 flex";
  messageElement.classList.add(position);

  messageElement.appendChild(textElement);

  messages.appendChild(messageElement);
};

/* CHATS DISPLAY */
const currentChatName = document.getElementById("chatName");

const selectChat = (chatId, username) => {
  currentChatName.innerHTML = "";
  messages.innerHTML = "";

  const userName = document.createElement("h2");
  userName.innerHTML = username;
  userName.classList = "font-bold";

  const status = document.createElement("p");
  status.innerHTML = "Online";
  status.classList = "font-gray-400";

  currentChatName.appendChild(userName);
  currentChatName.appendChild(status);

  messageInput.focus();

  const receivedMessages = chatMessages.get(chatId);

  if (receivedMessages === undefined) return;

  receivedMessages.forEach((receivedMessage) => {
    printMessage(receivedMessage.message, receivedMessage.sender);
  });
};

const addChat = (chatId, username) => {
  const chatElement = document.createElement("li");

  chatElement.innerHTML = username;
  chatElement.id = chatId;

  chatElement.addEventListener("click", () => {
    currentChatId = chatId;
    selectChat(chatId, username);
  });

  chats.appendChild(chatElement);
};

const removeChat = (chatId) => {
  const chatElement = document.getElementById(chatId);

  if (chatElement === undefined) {
    return;
  }

  chatElement.remove();
};

/* MESSAGE LISTENERS */

const messages = document.getElementById("messages");
const chats = document.getElementById("chats");

const rememberChatMessage = (chatId, message, messageSender) => {
  let allMessages = chatMessages.get(chatId);

  if (allMessages === undefined) {
    allMessages = [];
  }

  allMessages.push({
    message: message,
    sender: messageSender,
  });
  chatMessages.set(chatId, allMessages);
};

socket.on("messageSend", (message, receivedFrom) => {
  //console.log(message, receivedFrom);

  rememberChatMessage(receivedFrom, message, receivedFrom);

  if (currentChatId === receivedFrom) {
    printMessage(message, receivedFrom);
  }
});

socket.on("newConnection", (connectionId, username) => {
  console.log(connectionId);
  addChat(connectionId, username);
});

socket.on("userDisconnected", (connectionId) => {
  console.log(connectionId);
  removeChat(connectionId);
});

socket.on("activeUsers", (users) => {
  users.forEach((user) => {
    console.log(users);
    addChat(user[0], user[1]);
  });
});
