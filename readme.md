# How to install this project

1. Open your terminal
2. Navigate to this folder
3. Run `npm install`

# How to start this project

1. Open your terminal
2. Navigate to this folder
3. Run `npm run start`
