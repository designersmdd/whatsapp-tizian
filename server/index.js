const express = require("express");
const { Server } = require("socket.io");
const { createServer } = require("http");
const {
  uniqueNamesGenerator,
  adjectives,
  colors,
  names,
  animals,
} = require("unique-names-generator");

const app = express();

const server = createServer(app);

const socketIo = new Server(server, {
  cors: {
    origin: "*",
  },
});

const connectedUsers = new Map();

socketIo.on("connection", (socket) => {
  console.log(socket.id);

  socketIo.to(socket.id).emit(
    "activeUsers",
    Array.from(connectedUsers).filter((user) => user[0] !== socket.id)
  );

  socket.on("requestUsername", () => {
    const generatedUsername = uniqueNamesGenerator({
      dictionaries: [names, ["the"], colors, animals],
      separator: " ",
    });

    connectedUsers.set(socket.id, generatedUsername);
    socket.broadcast.emit("newConnection", socket.id, generatedUsername);
    socketIo.to(socket.id).emit("newUsername", generatedUsername);
  });

  socket.on("myUsername", (username) => {
    connectedUsers.set(socket.id, username);
    socket.broadcast.emit("newConnection", socket.id, username);
  });

  socket.on("messageReceived", (message, chatId) => {
    //socket.broadcast.emit("messageSend", message);
    socketIo.to(chatId).emit("messageSend", message, socket.id);
  });

  socket.on("disconnect", () => {
    connectedUsers.delete(socket.id);
    socket.broadcast.emit("userDisconnected", socket.id);
  });
});

const port = process.env.PORT ?? 8000;
server.listen(port, () => {
  console.log(`Server is active on http://localhost:${port}`);
});
